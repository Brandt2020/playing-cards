
// Playing Cards
// Part 1 Logan Weyers
// Part 2 Zachary Brandt

#include <iostream>
#include <conio.h>
#include <string>


using namespace std;

enum Rank { Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace };

enum Suit { Heart, Diamond, Spade, Club }; 


struct Card {

	Rank Rank;
	Suit Suit;
};

void PrintCard(Card card)
{
	cout << "The ";
		
	switch (card.Rank)
	{
	case (Two):
		cout << "Two";
		break;
	case (Three):
		cout << "Three";
		break;
	case (Four):
		cout << "Four";
		break;
	case (Five):
		cout << "Five";
		break;
	case (Six):
		cout << "Six";
		break;
	case (Seven):
		cout << "Seven";
		break;
	case (Eight):
		cout << "Eight";
		break;
	case (Nine):
		cout << "Nine";
		break;
	case (Ten):
		cout << "Ten";
		break;
	case (Jack):
		cout << "Jack";
		break;
	case (Queen):
		cout << "Queen";
		break;
	case (King):
		cout << "King";
		break;
	case (Ace):
		cout << "Ace";
		break;
	}

	switch (card.Suit)
	{
	case (Heart):
		cout << " of " << "Hearts";
		break;
	case (Diamond):
		cout << " of " << "Diamonds";
		break;
	case (Spade):
		cout << " of " << "Spades";
		break;
	case (Club):
		cout << " of " << "Clubs";
		break;
	}
}

Card HighCard(Card &c1, Card &c2)
{
	if (c1.Rank > c2.Rank)
		return c1;
	else
		return c2;
}
int main()
{


	Card c1;
	c1.Rank = Jack;
	c1.Suit = Spade;

	Card c2;
	c2.Rank = Seven;
	c2.Suit = Club;

	PrintCard(HighCard(c1, c2));

	(void)_getch();
	return 0;
}
